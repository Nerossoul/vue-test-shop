import Api from "../api";
export const getCurrencyRate = async (isRequestActual) => {
  if (isRequestActual === undefined) {
    return await Api.getCurrencyRate();
  }
  if (isRequestActual.state) {
    const result = await Api.getCurrencyRate();
    if (isRequestActual.state) {
      return result;
    }
  }
  console.log("currency", isRequestActual);
  return 0;
};
