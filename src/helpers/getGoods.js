import Api from '../api'

export const getGoods = async (isRequestActual)=> {
    if (isRequestActual.state) {
        let {Success, Value} =  await Api.getData()
        if (Success) {
            const {Goods} = Value
            if (isRequestActual.state) {
                return Goods
            }
        }
    }
    return []
}