import Api from '../api'

export const getNames = async (isRequestActual)=> {
    if (isRequestActual.state) {
        let result =  await Api.getNames()
        if (result) {
            if (isRequestActual.state) {
                return result
            }
        }
    }
    return {}
}